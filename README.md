# README #

### Architecture ###
 
The current application is split into two main parts.  The back end is a RESTful web service that is written in C# using ASP.NET MVC.  It interacts with the database directly and can deliver data to clients formatted as JSON documents.  If you aren't familiar with JSON, it's basically javascript objects formatted as text.

The other main component this repo, a front-end app built using javascript.  We used Phaser to build our game, which just uses vanilla javascript.  The rest of the app is built using the AngularJS framework, which is a popular and powerful javascript library.  This component of the application allows user interaction.  It consumes the resources provided by the RESTful API described earlier.

The backend is currently deployed on AppHarbor at http://jmg.apphb.com/
You'll probably get a 403 if you hit that URL directly.  You can interact with the API by sending different types of HTTP requests (GET, POST, PUT, DELETE) to different routes.  For example, navigating to http://jmg.apphb.com/api/plants should send a GET request to the server and return you the list of Plants in the database.

The frontend is deployed at https://scratch-testing.herokuapp.com/
For those new to the project, you may want to click around the app and see where everything is.  You also might want to create user accounts for yourself.  If you do explore the app a little, open the developer console in your browser and look at the HTTP requests that are being sent.  This will help you understand how the app interacts with the API.

### Technologies and Resources ###

We used git for both version control and deployment.  If you weren't already using git, you will be now!  Seth should be giving everyone repo access soon.  Repositories for both the front-end and back-end are hosted on Bitbucket.  So far we've been using the GitFlow Workflow, which is just a nice model that keeps different features in different branches until they're ready for deployment.  The most important thing is that we aren't all just making commits to master and develop all willy nilly.  We can sort more of this out later.

Everybody will probably need at least a working knowledge of AngularJS.   There are all kinds of resources online where you can learn the basics.  My only caution is to avoid the W3Schools tutorial, because it's really crappy and will give a false impression of how Angular is actually used.
It's not a hard framework to learn, and it's really powerful.  That being said, it's a bit different and it can take some time to understand how the various components fit together if this is your first time working with it, so please don't delay in bringing yourself up to speed.

As mentioned previously, we're using Phaser for the game.  I think everyone is familiar with it, and it's pretty easy to pick up from the docs since it's just vanilla javascript.

As required in CS305, we've used Trello for progress tracking and all that.  I don't see any reason why not to keep going. I'll add you new guys to the Trello board.

For better communication, we used Slack chat for a short time.  We added a bunch of integrations with Bitbucket, and Trello, and maybe more so it eventually became really impractical to really use.  However, I still think we should use some kind of team chat to avoid big ugly strings of 6-person emails.  If anyone has any suggestions here, please voice them!  I was thinking maybe a different Slack channel or a Google hangout.

Setup and Development

Once you get repository access, you'll be able to pull down a local copy of the projects and get setup for development.  
I've been thinking, and unless you're working on the back-end, you hopefully won't need to setup Visual Studio or get IIS running.  Instead you can use the deployed backend during most development.  That should save some hassle.

To run the front-end project, you'll need a local web server.  Some web IDEs (such as JetBrains Webstorm) come with a web server built in.  If that's not the case for you, you'll need to install and configure a local web server.  Again, there are all kind of ways to do this if you want to Google and find an easy one.  If you happen to have NodeJS (or npm) installed, this SO answer gives an easy way to get one setup.

### Links 
*[Production front end](https://scratch-testing.herokuapp.com/)

*[Production API](http://jmg.apphb.com/)

*[Trello](https://trello.com/b/KRvBhCt5/scratch-project)

*[GitFlow branching model](http://nvie.com/posts/a-successful-git-branching-model/)