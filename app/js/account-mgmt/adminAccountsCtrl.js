app.controller("adminAccountsController", ['$scope', '$timeout', 'accountService', 'userService',
    function($scope, $timeout, accountService, userService){
        $scope.accounts = [];
        $scope.showArchived = "false";
        $scope.error = false;
        $scope.loading = false;

        $scope.options = {
            showArchived: false
        };

        $scope.currentUser = userService.user;

        // Stuff to control sorting table
        $scope.reverseOrder = false;
        $scope.orderOption = 'username';
        $scope.sort = function(item){
            if($scope.orderOption=='username'){
                return item.userName;
            }
            else if($scope.orderOption=='name'){
                return item.firstName;
            }
            else if($scope.orderOption=='dateCreated'){
                return new Date(item.registrationDate);
            }
            else if($scope.orderOption=='isAdmin'){
                return item.isAdmin;
            }
            else return item.isDeleted;
        };
        $scope.chooseSorting = function(orderingField){
            if($scope.orderOption == orderingField){
                $scope.reverseOrder = !$scope.reverseOrder;
            }
            else{
                $scope.orderOption = orderingField;
                $scope.reverseOrder = false;
            }
        };

        //Stuff to control filtering:
        $scope.shouldFilter = function(account){
            return !account.isDeleted || $scope.options.showArchived;
        };

        // fields for a new account
        $scope.data = {
            userName: null,
            confirmUsername: null,
            password: null,
            confirmPassword: null,
            firstName: null,
            lastName: null,
            isAdmin: true
        };


        var init = function(){
            $scope.loading = true;
            $scope.error = false;
            // get list of students to display:
            // Note: we get all students then apply the filter in the view (faster and sleeker in the long run)
            accountService.getAdmins()
                .then(function(accounts){
                    $scope.accounts = accounts;
                })
                .catch(function(error){
                    $scope.error = true;
                })
                .finally(function(){
                    $scope.loading = false;
                });
        };
        init();

        $scope.reload = function(){
            accountService.getAdmins()
                .then(function(accounts){
                    $scope.accounts = accounts;
                    $scope.error = false;
                })
                .catch(function(error){
                    $scope.error = true;
                });
        };

        $scope.addAccount = function(){
            if($scope.data && $scope.data.userName && $scope.data.confirmUsername
                && $scope.data.password && $scope.data.confirmPassword
                && $scope.data.firstName && $scope.data.lastName){

                if( $scope.data.userName !== $scope.data.confirmUsername){
                    $scope.addError = "Username and Confirm Username don't match!";
                } else if($scope.data.password !== $scope.data.confirmPassword){
                    $scope.addError = "Password and Confirm Password don't match!";
                } else {
                    var accountObj = {
                        "claims": [],
                        "logins": [],
                        "roles": [],
                        "lastName": $scope.data.lastName,
                        "firstName": $scope.data.firstName,
                        "isAdmin": $scope.data.isAdmin,
                        "email": null, //we don't need this since users' username is their email
                        "emailConfirmed": false,
                        //"passwordHash":"APvJImYS0AM/8hl6XWozSzx6NaJXvBFQ6g5mNd8xpsT0J26YJFqxy2QTV0jBZfRCtw==",
                        //"securityStamp":"4c122c47-2750-4922-8301-35ee3ea37b44",
                        "phoneNumber": null,
                        "phoneNumberConfirmed": false,
                        "twoFactorEnabled": false,
                        "lockoutEndDateUtc": null,
                        "lockoutEnabled": false,
                        "accessFailedCount": 0,
                        //"id":"aa239742-b43e-46fd-9c28-a4225d1b9eba", //DON'T include an ID.  These will be auto-generated for us
                        "userName": $scope.data.userName, // watch the case sensitivity here
                        "Password": $scope.data.password,   // and also here
                        "ConfirmPassword": $scope.data.confirmPassword
                    };
                    accountService.createUser(accountObj)
                        .then(function () {
                            $scope.addError = null;
                            Materialize.toast('Success!', 3000);
                            $('#addAccountModal').closeModal();
                        })
                        .catch(function (error) {
                            Materialize.toast('Failed!', 4000);
                            $scope.addError = "Account creation failed.  Please check that the username is unique and that the passwords match."
                        })
                        .finally($scope.reload);
                }
            }
            else{
                $scope.addError = "Invalid Form";
            }
        };


        $scope.demote = function(account){
            if($scope.currentUser.id != account.id){
                //simulate action:
                account.isAdmin = false;
                //check that there is at least one admin account
                var admins = _.filter($scope.accounts, function(item){
                    return item.isAdmin && !item.isDeleted;
                });
                if(admins.length){
                    //take action
                    accountService.modifyUser(account)
                        .then(function(){
                            Materialize.toast('Success!', 6000);
                        })
                        .catch(function(error){
                            Materialize.toast('Failed!', 6000);
                            $scope.actionError = "Could not connect to server.  Please try again";
                        })
                        .finally($scope.reload);
                }
                else{
                    //undo action:
                    account.isAdmin = !account.isAdmin;
                    Materialize.toast('Failed!', 6000);
                    $scope.actionError = "There must be at least one active admin account";
                }
            }
        };

        $scope.acknowledge = function(){
            $scope.addError = null;
        };


        $scope.acknowledgeActionError = function(){
            $scope.actionError = null;
        };

        //Note this uses actual ID not studentID
        $scope.archiveAccount = function(account){
            //simulate action:
            account.isDeleted = true;
            //check that there is at least one admin account
            var admins = _.filter($scope.accounts, function(item){
                return item.isAdmin && !item.isDeleted;
            });
            if(admins.length){
                //take action
                accountService.modifyUser(account)
                    .then(function(){
                        Materialize.toast('Success!', 6000);
                    })
                    .catch(function(error){
                        Materialize.toast('Failed!', 6000);
                        $scope.actionError = "Could not connect to server.  Please try again";
                    })
                    .finally($scope.reload);
            }
            else{
                //undo action:
                account.isDeleted = false;
                Materialize.toast('Failed!', 6000);
                $scope.actionError = "There must be at least one active admin account";
            }
        };

        $scope.unArchiveAccount = function(account){
            account.isDeleted = false;

            accountService.modifyUser(account)
                .then(function(){
                    Materialize.toast('Success!', 3000);
                })
                .catch(function(error){
                    Materialize.toast('Failed!', 3000);
                    $scope.actionError = "Could not connect to server.  Please try again";
                })
                .finally($scope.reload);
        };

        $scope.hasAccountsToShow = function(){
            var active = _.filter($scope.accounts, function(account){
                return account.isDeleted == false;
            });
            return (active.length>0) || ($scope.accounts.length>0 && $scope.options.showArchived);
        };

    }]);