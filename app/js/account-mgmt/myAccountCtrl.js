app.controller("myAccountController", ['$scope', '$log', 'accountService', 'userService', 'authService', 'navService',
    function($scope, $log, accountService, userService, authService, navService){

        $scope.user = userService.getUser();
        $scope.firstName = $scope.user.firstName;
        $scope.lastName = $scope.user.lastName;

        // data for password reset form
        $scope.passwordReset = {
            oldPass: null,
            newPass: null,
            confirmNewPass: null,
            error: null
        };

        $scope.logout = function(){
            authService.logout();
            navService.goToLogin();
        };

        $scope.oldFirstName = null;
        $scope.oldLastName = null;

        $scope.startNameChange = function(){
            $scope.oldFirstName = $scope.user.firstName;
            $scope.oldLastName = $scope.user.lastName;
            $scope.editName = true;
        };

        $scope.submitNameChange = function(){
            $scope.oldFirstName = null;
            $scope.oldLastName = null;
            $scope.loading = true;
            accountService.modifyUser($scope.user)
                .then(function(response){
                    Materialize.toast("Success!", 2000);
                }, function(error){
                    Materialize.toast("Failed!", 2000);
                })
                .finally(function(){
                    $scope.loading = false;
                });
            $scope.editName = false;
        };

        $scope.cancelNameChange = function(){
            $scope.user.firstName = $scope.oldFirstName;
            $scope.user.lastName = $scope.oldLastName;
            $scope.editName = false;
        };

        $scope.startPasswordChange = function(){
            $scope.editPass = true;
            $scope.resetPasswordData();
        };

        $scope.submitPasswordChange = function(){
            if(!$scope.passwordReset.oldPass){
                $scope.passwordReset.error = "Please enter your current password";
            } else if(!$scope.passwordReset.newPass){
                $scope.passwordReset.error = "Please enter a new password";
            } else if(!$scope.passwordReset.confirmNewPass){
                $scope.passwordReset.error = "Please confirm your new password";
            } else if($scope.passwordReset.newPass !== $scope.passwordReset.confirmNewPass){
                $scope.passwordReset.error = "New Password and Confirm Password don't match!";
            } else{
                $scope.loading = true;
                accountService.changePassword($scope.user, $scope.passwordReset.oldPass, $scope.passwordReset.newPass)
                    .then(function(){
                        Materialize.toast("Success!", 3000);
                        $scope.editPass = false;
                        $scope.resetPasswordData();
                    }, function(error){
                        $scope.passwordReset.error = error.data;
                    })
                    .finally(function(){
                        $scope.loading = false;
                    });
            }
        };

        $scope.cancelPasswordChange = function(){
            $scope.editPass = false;
            $scope.resetPasswordData();
        };

        $scope.resetPasswordData = function(){
            $scope.passwordReset = {
                oldPass: null,
                newPass: null,
                confirmNewPass: null,
                error: null
            };
        };
    }]);


