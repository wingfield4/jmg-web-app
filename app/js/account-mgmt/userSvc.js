﻿// This service is responsible for maintaining information about the currently logged in user

app.factory("userService", ['$log', 'accountService', 'localStorageService', function ($log, accountService, localStorage){
    var service = {
        user: null,
        saveTime: null
    };

    service.onLogin = function (username) {
        return accountService.getByUsername(username)
            .then(function(user){
                //$log.debug("got user: " + JSON.stringify(user));
                service.user = user;
            })
            .catch(function(error){
                $log.error("Error getting user from username!\n" + JSON.stringify(error));
                service.user = null;
            })
            .finally(service.save);
    };

    service.onLogout = function(){
        service.user = null;
        service.save();
    };

    service.getUser = function(){
        service.load();
        return service.user;
    };

    service.save = function(){
        localStorage.set('user', service.user);
        localStorage.set('saveTime', (new Date()));
    };

    service.load = function(){
        service.user = localStorage.get('user') || null;
        service.saveTime = localStorage.get('saveTime') || null;

        if( service.saveTime && (new Date) - (new Date(service.saveTime)) > 2*60*60*1000){ //If last login was over two hours ago, reset
            $log.warn("User session expired");
            service.user = null;
        }
    };

    service.isAdmin = function(){
        return service.user && service.user.isAdmin;
    };

    service.init = function(){
        service.load();
    };

    return service;
}]);