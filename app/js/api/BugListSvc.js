app.factory("bugListService", ['$log', '$http', '$q', 'CONFIG', function($log, $http, CONFIG){
    var service = {};

    //TODO: test all

    service.getAll = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + '/api/buglists';
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                $log.error(JSON.stringify(error));

                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getById = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + '/api/buglists/' + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                $log.error(JSON.stringify(error));

                deferred.reject(error);
            });

        return deferred.promise;
    };

    //We may not need this
    service.create = function(bugs) {
        //TODO
    };

    return service;
}]);