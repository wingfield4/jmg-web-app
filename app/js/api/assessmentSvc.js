app.factory("assessmentService", ['$log', '$http', 'CONFIG', '$q', function($log, $http, CONFIG, $q){
    var service = {};

    //TODO: test all

    service.getAll = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + '/api/assessments';
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                $log.error(JSON.stringify(error));

                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getAllUnfiltered = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + '/api/assessments/unfiltered';
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                $log.error(JSON.stringify(error));

                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getById = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/assessments/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                $log.error(JSON.stringify(error));

                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.create = function(assessment){
        var deferred = $q.defer();

        var url = CONFIG.API + '/api/assessments';
        $http.post(url, assessment)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                $log.error(JSON.stringify(error));

                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.buildAssessment = function(description, seedPackets, bugs){
        var assessment = {
            "bugs": bugs,
            "seedPackets": seedPackets,
            "description": description,
            "dateCreated": new Date(),
            "isDeleted": false
        };
        return assessment;
    };

    service.modify = function(id, assessment){
        var deferred = $q.defer();

        var url = CONFIG.API + '/api/assessments/' + id;
        $http.put(url, assessment)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                $log.error(JSON.stringify(error));

                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.delete = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + '/api/assessments/' + id;
        $http.delete(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                $log.error(JSON.stringify(error));

                deferred.reject(error);
            });

        return deferred.promise;
    };

    return service;
}]);