app.factory("plantService", ['$log', '$http', '$q', 'CONFIG', function($log, $http, $q, CONFIG){
    var service = {};

    service.getPlants = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/plants";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getPlant = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/plants/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getPlantParts = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/plantparts";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getPlantPart = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/plantparts/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getPlantLists = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/plantlists";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getPlantList = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/plantlists/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    return service;

}]);