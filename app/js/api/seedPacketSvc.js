app.factory("seedPacketService", ['$log', '$http', '$q', 'CONFIG', function($log, $http, $q, CONFIG){
    var service = {};

    service.getAll = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/seedPackets";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getSeedPacket = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/seedPacket/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getSeedPacketLists = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/seedPacketLists";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getSeedPacketList = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/seedPacketLists/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    return service;

}]);