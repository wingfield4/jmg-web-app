app.factory("soilService", ['$log', '$http', '$q', 'CONFIG', function($log, $http, $q, CONFIG){
    var service = {};

    //TODO: test these once we actually have soils

    service.getSoils = function(){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/soils";
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getSoil = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + "/api/soils/" + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });

        return deferred.promise;
    };

    return service;

}]);