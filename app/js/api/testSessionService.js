//Keeps track of the current game session.  I.E. what assessment is currently loaded, the sutdent taking the assessment, etc

app.factory("testSessionService", ['$log', '$http', '$q', 'CONFIG', function($log, $http, $q, CONFIG){

    var service = {};

    service.getAll = function(){
        $log.debug("Getting all test sessions");
        var deferred = $q.defer();

        var url = CONFIG.API + '/api/testsessions';
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                $log.error(JSON.stringify(error));

                deferred.reject(error);
            });

        return deferred.promise;
    };

    service.getById = function(id){
        var deferred = $q.defer();

        var url = CONFIG.API + '/api/testsessions/' + id;
        $http.get(url)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                $log.error(JSON.stringify(error));

                deferred.reject(error);
            });

        return deferred.promise;
    };

    //Remember test sessions must be attached to a student
    service.create = function(session, student){
        var deferred = $q.defer();

        var url = CONFIG.API + '/api/testsessions';
        var data = {
            session: session,
            student: student
        };
        $http.post(url, data)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                $log.error(JSON.stringify(error));

                deferred.reject(error);
            });

        return deferred.promise;
    };

    return service;

}]);