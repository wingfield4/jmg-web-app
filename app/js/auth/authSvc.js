﻿//Service to handle login, logout, and registration of new accounts

app.factory('authService', ['CONFIG', '$log', '$http', '$q', 'localStorageService', 'userService',
    function (CONFIG, $log, $http, $q, localStorageService, userService) {

        var serviceBase = CONFIG.API;
        var authServiceFactory = {};

        var _authentication = {
            isAuth: false,
            userName: ""
        };

        var _login = function (loginData) {
            $log.debug("Logging user in");

            var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

            var deferred = $q.defer();

            $http.post(serviceBase + 'gettoken', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
                .success(function (response) {

                    localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName });

                    _authentication.isAuth = true;
                    _authentication.userName = loginData.userName;

                    userService.onLogin(loginData.userName)
                        .finally(function(){
                            deferred.resolve(response);
                        });

                }).error(function (err) {
                    $log.error(JSON.stringify(err));
                    _logOut();
                    deferred.reject(err);
                });

            return deferred.promise;

        };

        var _logOut = function () {

            localStorageService.remove('authorizationData');

            _authentication.isAuth = false;
            _authentication.userName = "";

            userService.onLogout();

        };

        var _fillAuthData = function () {

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
            }

        };

        //authServiceFactory.saveRegistration = _saveRegistration;
        authServiceFactory.login = _login;
        authServiceFactory.logout = _logOut;
        authServiceFactory.fillAuthData = _fillAuthData;
        authServiceFactory.authentication = _authentication;

        return authServiceFactory;
    }]);