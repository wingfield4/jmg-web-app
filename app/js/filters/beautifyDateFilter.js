app.filter("beautifyDate", function(){
    return function(dateString){
        var date = new Date(dateString);
        var month = date.getMonth() + 1;
        month = (month < 10)? "0" + month : month;

        var day = date.getDate();
        day = (day < 10)? "0" + day : day;

        var year = date.getFullYear();

        return month + "-" + day + "-" + year;
    }
});