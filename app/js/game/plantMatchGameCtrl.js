//TODO this game calls "getBugs" everytime an answer is givne, which is bad!
//TODO Remove old images when runGame is called

app.controller("plantMatchGameController", ['$scope', '$rootScope', '$state', '$log', '$interval', 'plantService', 'imageService', 'gameService',
    'voiceService', 'currentTestService', 'dimensionsService', 'navService', 'settings',
    function($scope, $rootScope, $state, $log, $interval, plantService, imageService, gameService,
             voiceService, currentTestService, dimensionsService, navService, settings) {
        $scope.error = null;

        $scope.openModal = function(){
            $('#loadModal').openModal();
        };
        $scope.closeModal = function(){
            $('#loadModal').closeModal();
        };

        $scope.gameCompleted = currentTestService.status.plant_match;

        var answers = [];
        var exitButtonName = null;

        var expectedWidth = 800;
        var expectedHeight = 600;

        var width = dimensionsService.getWidth();
        var height = dimensionsService.getHeight();
        var navBarHeight = $("#jmg-navbar").height();
        var game = new Phaser.Game(width, height, Phaser.AUTO, 'game-container', {
            preload: preload,
            create: create,
            update: update
        });

        var gameControls = null;  // This will later be an object with controls to manipulate bugs and text
        //These will be changed as the game progresses
        $scope.correctPlant = null;
        $scope.correctPlantIndex = null;
        $scope.otherPlants = null;
        $scope.chosenPlant = null;
        $scope.chosenPlantIndex = null;

        function preload() {
            game.load.crossOrigin = 'anonymous';  //allows CORS requests to CDN
            $scope.openModal();
            //Here we load all assets for the game
            exitButtonName = gameService.loadImage(game, 'exitBtn', 'app/assets/images/UI/exit.png');
            //$scope.assessmentPlants = JSON.parse(JSON.stringify(currentTestService.getCurrentAssessment().bugs));
            var assessmentPackets = currentTestService.getCurrentAssessment().seedPackets;
            var assessmentsPlants = _.map(assessmentPackets, function(packet){
                return packet.plant;
            });
            $scope.assessmentPlants = JSON.parse(JSON.stringify(assessmentsPlants));
            $scope.allPlants = JSON.parse(JSON.stringify(assessmentsPlants));
            game.load.image("background", "app/assets/images/backdrop.png");
            voiceService.preloadRobot(game);
            //load image for each bug
            _.forEach($scope.allPlants, function (plant) {
                if(plant.images.length){
                    game.load.image(plant.plantName, imageService.imageGetters.getPlantMatchImage(plant).imageUri);
                }
            });
        }

        function create() {
            //Here we create game assets and position them on screen
            if (!$scope.error) {
                //Create background
                var bg = game.add.sprite(0, 0, "background");
                bg.scale.x = width / expectedWidth;
                bg.scale.y = height / expectedHeight;

                setupGame();
            }

            var exit = gameService.addSprite(game, 750, 550, 25, null, exitButtonName);
            exit.inputEnabled = true;
            exit.events.onInputDown.add(function(){
                if(prompt("Are you sure you wish to abort?  The student's progress will not be saved.\n" +
                        "Please enter the PIN to continue") == settings.pin){
                    //exit without saving answers
                    navService.goToGameLobby();
                }
            }, this);
        }

        function update() {
            //This function gets called every frame so we can check for events, update positions, etc
        }

        function setupGame() {
            $scope.totalQuestionsAsked = 0;
            $scope.totalCorrect = 0;


            /// CREATING A ROBOT
            var initialMsg = "Hello human. " +
                "Please help me ... ";
            voiceService.createRobot(game);
            voiceService.speakWithRobot(initialMsg, 0.9, 1,
                voiceService.robots.NEUTRAL, voiceService.robotLocations.BOTTOMRIGHT);


            //Create text overlays for the currentInstruction and current score
            var instruction = game.add.text(width * 0.03, height * 0.03, "FInd the ", {
                fontSize: Math.max(height * 32 / expectedHeight, 12),
                fill: '#FFFFFF'
            }); //white


            var score = game.add.text(width * 0.92, height * 0.03, "0/0", {
                fontSize: Math.max(height * 32 / expectedHeight, 12),
                fill: '#FFFFFF'
            });

            var feedback = game.add.text(width * 0.03, height * 0.8, "", {
                fontSize: Math.max(height * 32 / expectedHeight, 12),
                fill: '#FFFFFF'
            });

            var boxWidth = width * 0.30;
            var boxHeight = height * 0.30;
            var boxAspectRatio = (width * 0.30) / (height * 0.30);

            var plants = [null, null, null, null]; // will later hold the 4 bugs

            //This object allows us to modify the text and the four bugs and such in a black-box way without creating a bunch of global vars
            gameControls = {
                updateInstruction: function (text) {
                    instruction.text = text;
                    voiceService.speakWithRobot(text, 0.9, 1,
                        voiceService.robots.NEUTRAL, voiceService.robotLocations.BOTTOMRIGHT);
                },
                updateScore: function (text) {
                    score.text = text;
                },

                updateFeedback: function (feedbackMessage) {
                    feedback.text = feedbackMessage;
                },
                clearFeedback: function () {

                    feedback.text = "";
                },

                speakMessage: function (message) {
                    var promise = voiceService.speakWithRobot(message, 0.9, 1,
                        voiceService.robots.NEUTRAL, voiceService.robotLocations.BOTTOMRIGHT);
                },
                updatePlant: function (index, plantName) {
                    var i = index - 1;


                    if (plants[i]) {
                        plants[i].destroy();
                    }
                    var xPos = width * 0.15 + (i % 2) * (width * 0.35);
                    var yDown = (i >= 2) ? 1 : 0;
                    var yPos = height * 0.15 + yDown * (height * 0.35);
                    var plant = game.add.sprite(xPos, yPos, plantName);
                    var plantAR = plant.width / plant.height;
                    if (plantAR > boxAspectRatio) {
                        //scale bug to fit in width
                        plant.width = boxWidth;
                        plant.height = plant.width / plantAR;
                        //center bug vertically
                        var dy = boxHeight + yPos - (plant.y + plant.height);
                        plant.y = yPos + 0.5 * dy;
                    }
                    else {
                        //scale bug to fit in height
                        plant.height = boxHeight;
                        plant.width = plant.height * plantAR;
                        //center bug horizontally
                        var dx = boxWidth + xPos - (plant.x + plant.width);
                        plant.x = xPos + 0.5 * dx;
                    }

                    //attach tap controller:
                    plant.inputEnabled = true;
                    plant.events.onInputDown.add(function () {
                        choosePlant(index);
                    }, plant);
                    plants[i] = plant;
                }
            };

            //Kick things off!
            $scope.closeModal();
            takeStep();

        }

        //Takes a "step" in the game.  Takes bug from queue, changes text, and loads more bugs
        function takeStep(){
            if($scope.assessmentPlants.length){
                //do normal actions
                //get next correct bug randomly from queue
                var correctPlant = $scope.assessmentPlants.splice(_.random($scope.assessmentPlants.length - 1), 1)[0];
                var index = Math.floor(4 * Math.random()) + 1;  // between 1 and 4
                gameControls.updatePlant(index, correctPlant.plantName);
                $scope.correctPlant= correctPlant;
                $scope.correctPlantIndex = index;

                //get other plants from set of all other plants
                var otherPlants = _.filter($scope.allPlants, function(plant){

                    return plant.plantName != correctPlant.plantName;
                });

                var others = _.sampleSize(otherPlants, 3);

                for(var i = 0; i < others.length; i++){
                    var indx = (i >= index-1)? i+1 : i;
                    gameControls.updatePlant(indx+1, others[i].plantName);
                }
                $scope.otherPlants = others;
                gameControls.updateInstruction("Find the " + correctPlant.plantName);
            }
            else{

                //record queued answers
                if(!$scope.gameCompleted) {
                    _.forEach(answers, function (answer) {
                        currentTestService.recordAnswer(
                            answer.description, //description of the question
                            answer.studentChoice, //the student's choice
                            answer.correctChoice, //the correct choice
                            answer.chapter, // chapter in the JMG curriculum
                            answer.choices //array of strings listing all potential choices
                        );
                    });
                }

                //game is over!  Go back to lobby
                currentTestService.completePlantMatch();
                navService.goToGameLobby();
            }

        }

        function choosePlant(index){
            $scope.totalQuestionsAsked += 1;
            //called when student taps a bug at some index
            $scope.chosenPlantIndex = index;
            //index will be a number 1 through 4
            var correct = index==$scope.correctPlantIndex;

            if(correct){
                gameControls.updateFeedback("Correct! That is the " + $scope.correctPlant.plantName);
                gameControls.speakMessage("Correct! That is the " + $scope.correctPlant.plantName);

                $scope.totalCorrect += 1;
                $scope.chosenPlant = $scope.correctPlant;
            }
            else{
                var indx = (index >=$scope.correctPlantIndex)? index - 2 : index -1;
                $scope.chosenPlant = $scope.otherPlants[indx];
                gameControls.speakMessage("Incorrect. That is the " + $scope.chosenPlant.plantName);
                gameControls.updateFeedback("Incorrect. That is the " + $scope.chosenPlant.plantName);
            }

            //update score
            var score = "" + $scope.totalCorrect + "/" + $scope.totalQuestionsAsked;
            gameControls.updateScore(score);

            //Record answer:
            var choices = [$scope.correctPlant.plantName];
            _.forEach($scope.otherPlants, function (plant) {
                choices.push(plant.plantName);
            });
            answers.push({
                description: "Find the " + $scope.correctPlant.plantName,
                studentChoice: $scope.chosenPlant.plantName,
                correctChoice: $scope.correctPlant.plantName,
                chapter: 1,
                choices: choices
            });
            //Go to the next set of choices
            takeStep();
        }

        var currentState = $state.current.name;
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams){
                if(toState.name != currentState){
                    if(game && game.destroy){
                        game.destroy();
                        game = null;
                    }
                }
            });
    }]);


