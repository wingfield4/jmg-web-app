/*
    This service allows us a convenient way to implement the publish-subscribe design pattern
    Publishers can register to publish events to various channels.  Registered publishers must have a publish(channels) method.
    Subscribers can register to receive events on various channels.
 */

app.factory("pubSubService", [function(){
    var service = {};

    service.channels = {};

    service.subscribe = function(channel, listener){
        // create list of listeners if it doesn't exist yet.
        if(!service.channels[channel]){
            service.channels[channel] = [];
        }

        var index = service.channels[channel].push(listener);

        // return an object the caller can use to unsubscribe
        return {
            unsubscribe: function(){
                delete service.channels[topic][index];
            }
        }
    };

    service.publish = function(channel, eventObj){
        // if the topic doesn't exist, or if it has no listeners, then we're done
        if(!service.channels[channel] || !service.channels[channel].length) return;

        service.channels[channel].forEach(function(item){
            if(item){
                //call the function with the eventObj (or an empty object if no event was provided)
                item(eventObj || {});
            }
        })
    };

    /*
        Clears all listeners;
        Should be called when cleaning up after a state change
     */
    service.reset = function(){
        service.channels = {};
    };

    return service;

}]);