/*
 This service will provide convenience methods for text-to-speech
 We should at minimum have a method to speak text without the robot and a method to speak text with the robot
 */

//TODO build and test this service

app.factory("voiceService", ['$q', 'gameService', function($q, gameService){

    var service = {};
    //Potential locations for the robot to pop up;  you should pass one of these to the speakWithRobot() method
    service.robotLocations = {
        BOTTOM: 1, //bottom, horizontally centered
        LEFT: 2, //;eft, vertically centered
        TOP: 3, //top, horizontally centered
        RIGHT: 4, //right, vertically centered
        BOTTOMLEFT: 5,
        BOTTOMRIGHT: 6,
        TOPLEFT: 7,
        TOPRIGHT: 8
    };

    service.robotDimensions = {
        width: 75,
        height: 225
    };

    //Constants that allow caller to select the robot to use
    service.robots = {
        NEUTRAL: 1, //robot with yellow head light
        CORRECT: 2, //robot with green flashing head light
        INCORRECT: 3 //robot with red flashing head light
    };

    service.spritesheets = {
        neutral: null,
        correct: null,
        incorrect: null
    };

    service.sprites = {
        neutral: null,
        correct: null,
        incorrect: null
    };

    service.voice = "UK English Female"; //default voice for speech synthesis
    service.doneSpeaking = true;
    /*
     This method speaks the given message without showing the robot
     message is a string of text to speak
     rate is a number from 0 to 1.5 which will scale the rate at which the message is spoken
     pitch is a number of 0 to 2 which will scale the pitch of the voice
     onstart is a function - callback for when the speech starts.  This will mostly be used internally to this service
     Returns a promise that will resolve when the message is completed speaking
     */
    // Rate is a scaling factor for how quickly the message is spoken.  1 Will cause the message to be spoken at the normal rate
    service.speak = function(message, rate, pitch, onstart){
        //cancel anything currently in progress
        service.cancel();
        var deferred = $q.defer();
        responsiveVoice.speak(message, service.voice, {rate: (rate || 1), pitch: (pitch || 1),
            onend: function(){
                deferred.resolve();
            },
            onerror: function(){
                deferred.reject();
            },
            onstart: onstart || function(){}
        });

        return deferred.promise;
    };

    /*
     This method will cancel any currently in-progress messages being spoken
     If the robot is loaded, the robot will be moved off-screen
     */
    service.cancel = function(){
        responsiveVoice.cancel();
        if(service.sprites.neutral){
            gameService.changePosition(service.sprites.neutral, -1500, -1500, 0);
        }
        if(service.sprites.correct){
            gameService.changePosition(service.sprites.correct, -1500, -1500, 0);
        }
        if(service.sprites.incorrect){
            gameService.changePosition(service.sprites.incorrect, -1500, -1500, 0);
        }
    };


    /*
     This method should be called in the game's preload method
     This must be called prior to calling speakWithRobot()
     */
    service.preloadRobot = function(game){
        service.spritesheets.neutral = gameService.loadSpriteSheet(game, {name: "neutral_robot_sheet", path: "app/assets/spritesheets//robot/talk_spritesheet.png", width: 195, height: 304});
        service.spritesheets.correct = gameService.loadSpriteSheet(game, {name: "correct_robot_sheet", path: "app/assets/spritesheets//robot/correct_spritesheet.png", width: 195, height: 304});
        service.spritesheets.incorrect = gameService.loadSpriteSheet(game, {name: "incorrect_robot_sheet", path: "app/assets/spritesheets//robot/incorrect_spritesheet.png", width: 195, height: 304});
    };

    /*
     This method should be called in the game's create method
     This must be called prior to calling speakWithRobot()
     */
    service.createRobot = function(game){
        //load the sprite for each of the three "robots" (correct, incorrect, and neutral)
        //neutral
        service.sprites.neutral = gameService.addSprite(game,
            -1500, -1500, //the sprite starts off screen - it will be moved into the view area when it's time to display the robot
            service.robotDimensions.width, service.robotDimensions.height,
            service.spritesheets.neutral,
            true
        );
        service.sprites.neutral.animations.add('move');
        service.sprites.neutral.play('move', 10, true);
        //correct
        service.sprites.correct = gameService.addSprite(game,
            -1500, -1500, //the sprite starts off screen - it will be moved into the view area when it's time to display the robot
            service.robotDimensions.width, service.robotDimensions.height,
            service.spritesheets.correct,
            true
        );
        service.sprites.correct.animations.add('move');
        service.sprites.correct.play('move', 10, true);
        //incorrect
        service.sprites.incorrect = gameService.addSprite(game,
            -1500, -1500, //the sprite starts off screen - it will be moved into the view area when it's time to display the robot
            service.robotDimensions.width, service.robotDimensions.height,
            service.spritesheets.incorrect,
            true
        );
        service.sprites.incorrect.animations.add('move');
        service.sprites.incorrect.play('move', 10, true);
    };

    /*
     TODO: stub
     This method speaks the given message while showing the robot in the specified location
     message: string: text to be spoken
     robot: numeric code of which robot to use.  Can be accessed via service.robots.NEUTRAL for example
     location: numeric code where the robot will popup.  Can be accessed via service.robotLocations.LEFT for example
     returns a promise that is resolved when the robot is done speaking
     */
    service.speakWithRobot = function(message, rate, pitch, robot, location){
        service.doneSpeaking = false;
        //determine robot sprite and position
        var sprite = service.sprites.neutral;
        if(robot == service.robots.CORRECT){sprite = service.sprites.correct;}
        if(robot == service.robots.INCORRECT){sprite = service.sprites.incorrect;}

        var x, y, angle;
        if(location == service.robotLocations.BOTTOM){x = 400; y = 600 - service.robotDimensions.height*0.50; angle = 0;}
        if(location == service.robotLocations.LEFT){x = service.robotDimensions.height*0.25; y = 300; angle=Math.PI * 0.5;}
        if(location == service.robotLocations.TOP){x = 400; y = service.robotDimensions.height*0.23; angle=Math.PI}
        if(location == service.robotLocations.RIGHT){x = 800 - service.robotDimensions.height*0.25; y = 300; angle=Math.PI * 1.5}
        if(location == service.robotLocations.BOTTOMLEFT){x = service.robotDimensions.width*0.5; y = 600 - service.robotDimensions.height*0.50; angle=Math.PI * 0.25}
        if(location == service.robotLocations.BOTTOMRIGHT){x = 800 - service.robotDimensions.width*0.50; y = 600 - service.robotDimensions.height*0.50; angle=Math.PI * 1.75}
        if(location == service.robotLocations.TOPLEFT){x = service.robotDimensions.width*0.50; y = service.robotDimensions.height*0.25; angle=Math.PI * 0.75}
        if(location == service.robotLocations.TOPRIGHT){x = 800 - service.robotDimensions.width*0.50; y = service.robotDimensions.height*0.25; angle=Math.PI * 1.25}

        //callback function for when speech is begun
        function onstart(){
            //once speech has started, move robot into position
            gameService.changePosition(sprite, x, y, angle);
        }

        //start the speech
        return service.speak(message, rate, pitch, onstart)
            .then(function(){
                service.doneSpeaking = true;
                console.log("Finished speaking?" + service.doneSpeaking);

                //hide sprite when message is done
                sprite.x = -1500;
                sprite.y = -1500;
                sprite.angle = 0;
            });
    };

    return service;
}]);