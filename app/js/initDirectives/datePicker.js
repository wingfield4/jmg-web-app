app.directive('datePickerInit', ['$timeout', function($timeout){
    return{
        restrict:'A',
        link:function(scope,elem,attrs){
            $timeout(function(){
                elem.pickadate({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 20 // Creates a dropdown of 15 years to control year
                });
            },0)
        }
    }
}]);