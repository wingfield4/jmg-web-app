app.directive('dropdownInit', ['$timeout', function($timeout){
    return{
        restrict:'A',
        link:function(scope,elem,attrs){
            $timeout(function(){
                elem.dropdown();
            },0)
        }
    }
}]);