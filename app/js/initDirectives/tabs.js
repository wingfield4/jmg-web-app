app.directive('tabsInit', ['$timeout', function($timeout){
    return{
        restrict:'A',
        link:function(scope,elem,attrs){
            $timeout(function(){
                elem.tabs();
            },0)
        }
    }
}]);