app.controller("viewReportsController", ['$scope', '$log', '$stateParams', 'navService', 'reportService',
function($scope, $log, $stateParams, navService, reportService){
    $scope.report = $stateParams.report;

    if(!$scope.report){
        $log.debug("Going back to report generation");
        navService.goToReports();
    }

    $scope.errors = $stateParams.report.errors;

    if($scope.errors.length){
        $('#error-modal').openModal();
    }

    $scope.reverseOrder = false;
    $scope.orderOption = 'studentID';
    $scope.sort = function(item){
        if($scope.orderOption=='pctCorrect'){
            return item.pctCorrect;
        }
        else if($scope.orderOption=='correctCount'){
            return item.correctCount;
        }
        else if($scope.orderOption=='incorrectCount'){
            return item.incorrectCount;
        }
        else if($scope.orderOption=='bestChapter'){
            return item.bestChapter;
        }
        else if($scope.orderOption=='worstChapter'){
            return item.worstChapter;
        }
        else if($scope.orderOption=='averageTime'){
            return Number(item.minutes) + Number(item.seconds)/60;
        }
        else return item.studentID
    };

    $scope.chooseSorting = function(attr){
        if($scope.orderOption == attr){
            $scope.reverseOrder = !$scope.reverseOrder;
        }
        else{
            $scope.reverseOrder = false;
            $scope.orderOption=attr;
        }
    };

    $scope.goToStudentDetail = function(studID){
        //find student with specified id:
        _.forEach($scope.report.results, function(result){
            if(result.studentID == studID){
                navService.goToStudentDetailReport(result, $scope.report, studID);
            }
        });
    };

    //Format of CSV: {studentID, answercount, question, studentResponse, correctResponse, chapter, choices (space-separated), ...}
    $scope.downloadCSV = function(){
        //The top row labels the columns
        var csvString = "student_id, question, chapter, ordering, answer, is_correct, choices, question_time\n";
        _.forEach($scope.report.results, function(student){
            var allAnswers = [];
            _.forEach(student.chapterResults, function(result){
                allAnswers = allAnswers.concat(result.answers);
            });
            _.forEach(allAnswers, function(answer){
                csvString += student.studentID + ",";
                csvString += answer.question + ",";
                csvString += answer.chapter + ",";
                csvString += answer.answerID + ",";
                csvString += answer.studentChoice + ",";
                //detects if answer was correct
                csvString += (reportService.isChoiceCorrect(answer.correctChoice, answer.studentChoice)) + ",";
                csvString += answer.choices.replace(/,/g, "") + ",";
                csvString += answer.timestamp;
                csvString += "\n";
            });
        });

        var filename = "report.csv";
        var blob = new Blob([csvString], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            } else{
                //Fallback for browser's that don't support for the "download" attr
                var csvURI = encodeURI(csvString);
                window.open(csvURI);
            }
        }
    };

    $scope.closeModal = function(){
        $('#error-modal').closeModal();
    };

    $scope.goBack = function(){
        $scope.closeModal();
        navService.goToReports();
    };
}]);

app.filter("formatSeconds", function(){
    return function(seconds){
        var formatted = seconds;
        if(Number(seconds) < 10){
            formatted = "0" + formatted;
        }
        return formatted;

    };
});