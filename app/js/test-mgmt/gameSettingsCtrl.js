app.controller('gameSettingsController', ['$scope', 'gameSettingsService', function($scope, gameSettingsService){
    $scope.loading = true;
    $scope.error = null;
    $scope.clearError = function(){$scope.error = null};

    $scope.settings = {};

    $scope.load = function(){
        $scope.loading = true;
        gameSettingsService.getAll()
            .then(function(data){
                $scope.settings = data[0];
            }, function(error){
                $scope.error = "Error loading game settings";
            })
            .finally(function(){
                $scope.loading = false;
            })
    };
    $scope.load();

    $scope.update = function(){
        if($scope.settings){
            $scope.loading = true;
            gameSettingsService.updateSettings($scope.settings.id, $scope.settings)
                .then(function(data){
                    $scope.settings = data;
                }, function(error){
                    $scope.error = "Error updating game settings!";
                })
                .finally(function(){
                    $scope.loading = false;
                })
        }
    };
}]);