app.controller("testMgmtController", ['$log', '$q', '$scope', 'assessmentService', 'bugService', 'seedPacketService', 'imageService',
    function($log, $q, $scope, assessmentService, bugService, seedPacketService, imageService){

        $scope.loading = null;
        $scope.error = null;
        $scope.clearError = function(){$scope.error = null};

        //Will hold unparsed JSON strings from select tag's ng-model
        $scope.selectedObjects = {
            selectedGameObj: null,
            selectedAssessmentObj: null,
            selected: null
        };

        $scope.selectedGame = null;
        $scope.$watch('selectedObjects.selectedGameObj', function(){
            $scope.selectedGame = JSON.parse($scope.selectedObjects.selectedGameObj)
        }, true);
        $scope.games = [
            {
                name: "Bug Sort Game",
                chapter: 4,
                id: 1
            },
            {
                name: "Bug Match Game",
                chapter: 4,
                id: 2
            },
            {
                name: "Vegetable Match Game",
                chapter: 6,
                id: 3
            },
            {
                name: "Fruit-Vegetable Game",
                chapter: 6,
                id: 4
            }
        ];
        $scope.selectedAssessment = null;
        $scope.selectedAssessmentObj = null;
        $scope.$watch('selectedObjects.selectedAssessmentObj', function(){
            $scope.selectedAssessment = JSON.parse($scope.selectedObjects.selectedAssessmentObj)
        }, true);
        $scope.assessments = [];

        $scope.BUG_TYPES = {
            0: "Predator",
            1: "Pollinator",
            2: "Pooper",
            3: "Pest"
        };
        $scope.allBugs = [];
        $scope.allVegs = [];

        $scope.imageGetters = imageService.imageGetters;

        $scope.load = function(){
            $scope.loading = true;
            var pm1 = assessmentService.getAll()
                .then(function(assessments){
                    $scope.assessments = assessments;
                }, function(error){
                    $scope.error = "Failed to load assessments";
                });
            var pm2 = bugService.getAll()
                .then(function(bugs){
                    $scope.allBugs = bugs;
                },function(error){
                    $scope.error = "Failed to load bugs";
                });
            var pm3 = seedPacketService.getAll()
                .then(function(packets){
                    $scope.allVegs = packets;
                }, function(error){
                    $scope.error = "Failed to load vegetables";
                });

            $q.all([pm1, pm2, pm3]).finally(function(){
                $scope.loading = false;
            })
        };
        $scope.load();

        //determines if the bug belongs to the selected assessment
        $scope.bugIsIncluded = function(bug){
            return _.findIndex($scope.selectedAssessment.bugs, function(b){
                    return b.bugID == bug.bugID
                }) >= 0;
        };
        $scope.bugIsExcluded = function(bug){
            return !$scope.bugIsIncluded(bug);
        };

        //determines if the seed packet is a part of the currently selected assessment
        $scope.vegIsIncluded = function(packet){
            return _.findIndex($scope.selectedAssessment.seedPackets, function(p){
                    return p.seedPacketID == packet.seedPacketID
                }) >= 0;
        };
        $scope.vegIsExcluded = function(packet){
            return !$scope.vegIsIncluded(packet);
        };

        $scope.removeBug = function(bug){
            $scope.loading = true;
            _.remove($scope.selectedAssessment.bugs, function(b){
                return b.bugID == bug.bugID;
            });
            assessmentService.modify($scope.selectedAssessment.assessmentID, $scope.selectedAssessment)
                .then(function(modified){
                    Materialize.toast("Success!", 2500);
                    $scope.selectedAssessment = modified;
                }, function(error){
                    Materialize.toast("Failed!", 3000);
                    $scope.selectedAssessment.bugs.push(bug);
                })
                .finally(function(){
                    $scope.loading = false
                });
        };

        $scope.addBug = function(bug){
            $scope.loading = true;
            $scope.selectedAssessment.bugs.push(bug);
            assessmentService.modify($scope.selectedAssessment.assessmentID, $scope.selectedAssessment)
                .then(function(modified){
                    Materialize.toast("Success!", 2500);
                    $scope.selectedAssessment = modified;
                }, function(error){
                    Materialize.toast("Failed!", 3000);
                    _.remove($scope.selectedAssessment.bugs, function(b){
                        return b.bugID == bug.bugID;
                    });
                })
                .finally(function(){
                    $scope.loading = false
                });
        };

        $scope.toggleBugIncluded = function(bug){
            if($scope.bugIsIncluded(bug)){
                $scope.removeBug(bug);
            } else{
                $scope.addBug(bug);
            }
        };

        $scope.removePacket = function(packet){
            $scope.loading = true;
            _.remove($scope.selectedAssessment.seedPackets, function(p){
                return p.seedPacketID == packet.seedPacketID;
            });
            assessmentService.modify($scope.selectedAssessment.assessmentID, $scope.selectedAssessment)
                .then(function(modified){
                    Materialize.toast("Success!", 2500);
                    $scope.selectedAssessment = modified;
                }, function(error){
                    Materialize.toast("Failed!", 3000);
                    $scope.selectedAssessment.seedPackets.push(packet);
                })
                .finally(function(){
                    $scope.loading = false
                });
        };

        $scope.addPacket = function(bug){
            $scope.loading = true;
            $scope.selectedAssessment.seedPackets.push(bug);
            assessmentService.modify($scope.selectedAssessment.assessmentID, $scope.selectedAssessment)
                .then(function(modified){
                    Materialize.toast("Success!", 2500);
                    $scope.selectedAssessment = modified;
                }, function(error){
                    Materialize.toast("Failed!", 3000);
                    _.remove($scope.selectedAssessment.seedPackets, function(p){
                        return p.seedPacketID == packet.seedPacketID;
                    });
                })
                .finally(function(){
                    $scope.loading = false
                });
        };

        //Really veg is a seed packet
        $scope.toggleVegIncluded = function(veg){
            if($scope.vegIsIncluded(veg)){
                $scope.removePacket(veg);
            } else{
                $scope.addPacket(veg);
            }
        };

        //sets the selected object for the modals
        $scope.pickItem = function(item){
            $scope.selectedObjects.selected = item;
        }
    }]);