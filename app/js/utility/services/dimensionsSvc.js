app.factory("dimensionsService", [function(){
    var service = {};

    service.getHeight = function(){
        return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    };

    service.getWidth = function(){
        return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    };

    return service;
}]);