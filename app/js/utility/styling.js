//Directives that can be used to style elements
//We can change our site's "theme" here

app.directive("primary", [function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            element.addClass('blue darken-4')
        }
    };
}])

    .directive("secondary", [function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                element.addClass('blue lighten-3')
            }
        };
    }])

    //mostly using this for buttons
    .directive("tertiary", [function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                element.addClass('green')
            }
        };
    }])

    .directive("light", [function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                element.addClass('light-green lighten-4')
            }
        };
    }])

    .directive("dark", [function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                element.addClass('grey darken-3')
            }
        };
    }])

    .directive("textPrimary", [function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                element.addClass('blue-text')
            }
        };
    }])

    .directive("textSecondary", [function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                element.addClass('blue-text text-lighten-5')
            }
        };
    }])

    .directive("textTertiary", [function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                element.addClass('blue-text')
            }
        };
    }])

    .directive("textLight", [function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                element.addClass('white-text')
            }
        };
    }])

    .directive("textDark", [function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                element.addClass('grey-text text-darken-3')
            }
        };
    }]);